#!/bin/bash

set -e

if [ "$1" = 'redis' ]; then
    exec /usr/bin/redis-server /etc/redis/redis.conf
fi

exec "$@"